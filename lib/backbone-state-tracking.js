'use strict';

/* Inspired by https://github.com/hernantz/backbone.sos */
const LOADING_EVENTS = 'request';
const LOADED_EVENTS = 'sync error reset';

function loadingChange(obj, newState) {
  newState = Boolean(newState);
  const current = Boolean(obj.loading);
  obj.loading = newState;
  if (newState !== current) {
    obj.trigger(newState ? 'loading' : 'loaded', obj);
    obj.trigger('loading:change', obj, newState);
  }
}

function onLoading() {
  loadingChange(this, true);
}

function onLoaded() {
  loadingChange(this, false);
}

module.exports = {
  track(model) {
    model.loading = false;
    model.stateTracking = true;
    model.listenTo(model, LOADING_EVENTS, onLoading);
    model.listenTo(model, LOADED_EVENTS, onLoaded);
  },
  untrack(model) {
    model.stateTracking = false;
    model.stopListening(model, LOADING_EVENTS, onLoading);
    model.stopListening(model, LOADED_EVENTS, onLoaded);
  },
};
