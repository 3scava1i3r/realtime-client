'use strict';

const Backbone = require('backbone');
const _ = require('underscore');

const LimitedCollection = Backbone.Collection.extend({
  // eslint-disable-next-line object-shorthand
  constructor: function(models, options) {
    if (!options || !options.collection) {
      throw new Error('A valid collection must be passed to a new instance of LimitedCollection');
    }

    this._collection = options.collection;
    this._maxLength = options.maxLength || 10;

    const resetModels = this._collection.slice(0, this._maxLength);

    Backbone.Collection.call(
      this,
      resetModels,
      _.extend({}, options, {
        comparator: this._collection.comparator,
      }),
    );

    this.listenTo(this._collection, 'add', this._onAddEvent);
    this.listenTo(this._collection, 'remove', this._onRemoveEvent);
    this.listenTo(this._collection, 'reset', this._resetFromBase);
    this.listenTo(this._collection, 'sort', this._resetFromSort);
  },

  _onAddEvent(model /* , collection, options */) {
    const index = this._collection.indexOf(model);
    if (index >= this._maxLength) {
      return;
    }

    return this.add(model);
  },

  _onRemoveEvent(model /* , collection, options */) {
    const didRemove = this.remove(model);
    if (didRemove) {
      this._resetFromSort();
    }
  },

  _resetFromBase() {
    const resetModels = this._collection.slice(0, this._maxLength);
    this.comparator = this._collection.comparator;

    return this.reset(resetModels);
  },

  _resetFromSort() {
    const resetModels = this._collection.slice(0, this._maxLength);
    this.comparator = this._collection.comparator;
    this.set(resetModels, {
      add: true,
      remote: true,
      merge: false,
    });
  },

  getUnderlying() {
    return this._collection;
  },
});

module.exports = LimitedCollection;
