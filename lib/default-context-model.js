'use strict';

const Backbone = require('backbone');

// Create a context model for backwards compatibility for clients
// which have not supplied one. The default context model only binds
// against userId, and only does so once
module.exports = function(client, suppliedUserId) {
  const userId = suppliedUserId || client.getUserId();
  const contextModel = new Backbone.Model({ userId });

  if (!userId) {
    client.on('change:userId', userId => {
      contextModel.set({ userId });
    });
  }

  return contextModel;
};
