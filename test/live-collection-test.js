'use strict';

const assert = require('assert');
const LiveCollection = require('../lib/live-collection');

describe('LiveCollection', () => {
  it('handleSnapshot should add, not replace the collection', () => {
    const liveCollection = new LiveCollection([{ id: 1 }, { id: 2 }], {
      client: { getUserId: () => {}, on: () => {} },
      url: '/test/',
    });
    liveCollection.handleSnapshot([{ id: 3 }]);
    assert.equal(liveCollection.length, 3);
  });
});
